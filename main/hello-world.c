#include <gtk/gtk.h>

static void on_app_activate (GApplication *app, gpointer data)
{
  GtkWidget *window = gtk_application_window_new (GTK_APPLICATION (app));

  GtkWidget *label = gtk_label_new ("Hello, World!");

  gtk_container_add (GTK_CONTAINER(window), label);

  gtk_widget_show_all (GTK_WIDGET(window));
}

int main(int argc, char *argv[])
{
  GtkApplication *app = gtk_application_new (
      "com.codethink.gtk-hello",
      G_APPLICATION_FLAGS_NONE
  );

  g_signal_connect (app, "activate", G_CALLBACK(on_app_activate), NULL);

  int status = g_application_run(G_APPLICATION(app), argc, argv);

  g_object_unref (app);

  return status;
}

# bazel-gtk-hello

An example repo to get bazel to build a GUI app, written using GTK.

## Using

To build this app, you first need to import the binaries and headers from your
system. Ensure you have gtk3 installed from your distribution, and run

```shell
./copy-deps.sh
```

This should position all of the necessary dependencies and headers from your
host into the tree, ready to be imported and used in the bazel build.

You can run the app using

```shell
./bazel-bin/main/gtk-hello
```

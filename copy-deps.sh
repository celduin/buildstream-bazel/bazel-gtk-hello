#!/usr/bin/env bash

headers=$(pkg-config --cflags-only-I gtk+-3.0 | sed 's/\-I//g')
libs=$(pkg-config --libs-only-l gtk+-3.0 | sed 's/\-l//g')

includedir=main/include
libdir=main/lib

mkdir -p $includedir
mkdir -p $libdir

echo "###########################################"
echo "#        Copying headers into tree        #"
echo "###########################################"
echo

for include in $headers
do
  target=$(sed 's|/usr|main|; s|/x86_64-linux-gnu||' <<< $include)
	echo "Copying $include to $target\n"
	mkdir -p $(dirname $target)
	cp -r $include $target
done

echo
echo
echo "###########################################"
echo "#        Copying binaries into tree       #"
echo "###########################################"
echo

for lib in $libs
do
	echo 'void main() {}' | gcc -x c -shared -l$lib -o $lib.out
	src=$(ldd $lib.out | grep $lib | awk '{print $3}')
	target=$libdir/lib$lib.so
	echo "Copying $src to $target"
	cp $src $target
	rm $lib.out
done
